#include <iostream>
#include "Lista.h"

using namespace std;

// Constructor por defecto
Lista::Lista() {}

// Funcion que crea y enlaza nodos
void Lista::crear(int numero){

    Nodo *temp = new Nodo;
    temp->numero = numero;

    if (this->primero == NULL){
        this->primero = temp;
        this->ultimo = temp;
    }
    else{

        Nodo *actual = this->primero;

        while(actual != NULL){

            if (temp->numero <= actual->numero){
                this->primero = temp;
                temp->sig = actual;
                break;
            }

            else if (temp->numero > this->ultimo->numero){
                this->ultimo->sig = temp;
                this->ultimo = temp;
                break;
            }

            else if (temp->numero >= actual->numero && temp->numero <= actual->sig->numero){
                temp->sig = actual->sig;
                actual->sig = temp;
                break;
            }
            actual = actual->sig;
        }
    }
}

// Funcion que imprime lista de menor a mayor
void Lista::imprimir(){

    Nodo *aux = this->primero;

    while (aux != NULL){
        cout << "(" << aux->numero << ")->";
        aux = aux->sig;
    }
    cout << "FIN" << endl << endl;
}
