#include <iostream>
#include "Nodo.h"

using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista{

    // Atributos de nodo
    private:
        Nodo *primero = NULL;
        Nodo *ultimo = NULL;

    public:
        // Constructor default
        Lista();
        // Funcion que crea nuevos nodos
        void crear(int numero);
        // Funcion que imprimie la lista
        void imprimir();
};
#endif
