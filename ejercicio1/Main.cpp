#include <iostream>
#include "Lista.h"

using namespace std;

int main(){

    Lista lista;
    string cantidad;
    string numero;

    cout << "\n\t= Listas enlazadas simples =" << endl;
    cout << "\n¿Cuántos números desea ingresar?: ";
    getline(cin, cantidad);

    if (stoi(cantidad) == 0){
        cout << "Hasta pronto!" << endl;
    }

    else{
        for (int i=0; i<stoi(cantidad); i++){

            cout << "Ingrese número para añadir a la lista: ";
            getline(cin, numero);
            lista.crear(stoi(numero));
            cout << endl << "\t< Lista actual >" << endl;
            lista.imprimir();
        }
    }

    return 0;
}
