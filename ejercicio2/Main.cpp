#include <iostream>
#include "Lista.h"

using namespace std;

void juntar_listas(Lista *listas){

    Nodo *aux = NULL;

    for(int i = 0; i < 2; i++){

        cout << "Lista " << (i+1) << ":" << endl;
        listas[i].imprimir();
        aux = listas[i].get_primero();

        while(aux != NULL){
            listas[2].crear(aux->numero);
            aux = aux->sig;
        }
    }

    cout << "Lista 3 creada al juntar listas 1 y 2:" << endl;
    listas[2].imprimir();
}

void llenar_listas(Lista *listas){

    string cantidad;
    string numero;

    for(int i = 0; i < 2; i++){

        cout << "¿Cuántos números desea ingresar a la lista " << (i+1) << "?: ";
        getline(cin, cantidad);

        for(int j = 0; j < stoi(cantidad); j++){

            cout << endl << "¿Que número desea ingresar?: ";
            getline(cin, numero);

            listas[i].crear(stoi(numero));
        }
        cout << endl << "¡Lista " << (i+1) << " creada con éxito!" << endl << endl;
    }
}

int main(){

    Lista listas[3];
    llenar_listas(listas);
    juntar_listas(listas);

    return 0;
}
