#include <iostream>
#include "Nodo.h"

using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista{

    private:
        Nodo *primero = NULL;
        Nodo *ultimo = NULL;

    public:
        // Constructor por defecto
        Lista();

        void crear(int numero);
        void imprimir();
        Nodo *get_primero();
};
#endif
