#include <iostream>
#include "Lista.h"

using namespace std;

void rellenar_espacios(Lista &lista){

    Nodo *aux = lista.get_primero();

    while(aux->sig != NULL){

        if((aux->numero + 1) != (aux->sig->numero)){

            for(int i=aux->numero+1; i<aux->sig->numero; i++){
                lista.crear(i);
            }
        }
        aux = aux->sig;
    }
    cout << "Se han llenado los espacios exitosamente!" << endl << endl;
}

void llenar_lista(Lista &lista){

    string numero;
    string cantidad;

    cout << "¿Cuantos números desea añadir a la lista?: ";
    getline(cin, cantidad);

    if (stoi(cantidad) == 0){
        cout << "\n¡Hasta pronto!" << endl;
    }
    else{

        for(int i = 0; i < stoi(cantidad); i++){

            cout << "Ingrese número para añadir a la lista: ";
            getline(cin, numero);

            lista.crear(stoi(numero));
        }
        cout << endl << "Lista creada exitosamente!" << endl << endl;
    }
}

int main(){

    Lista lista;

    llenar_lista(lista);
    cout << "La lista creada es la siguiente:" << endl;
    lista.imprimir();

    rellenar_espacios(lista);
    cout << "La nueva lista con los espacios rellenados es la siguiente:" << endl;
    lista.imprimir();

    return 0;
}
