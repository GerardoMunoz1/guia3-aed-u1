#include <iostream>
#include "Nodo.h"

using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista{

    private:
        Nodo *primero = NULL;
        Nodo *ultimo = NULL;

    public:
        // Constructor por defecto
        Lista();
        // Función que permite la creación y enlace de nuevos nodos
        void crear(int numero);
        // Función que imprime nodos de la lista actual
        void imprimir();
        // Función que permite obtener el primer nodo para recorrer la lista
        // desde el principio
        Nodo *get_primero();
};
#endif
